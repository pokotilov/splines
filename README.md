Generic splines library support different type of splines (Bezier, Hermite, Catmull-Rom, B-Spline).
You can use almost arbitrary type as interpolating value.

Library provides common set of operations:

* interpolation, derivatives calculation, frenet frame calculation

* natural (arclength) parametrization

* distance to point, obtaining corresponding spline closest point

File design.txt contains more inforamtion about library design.